;; TODO:
;; * packaging.
;; * implement docs
;; * implement ramped half-and-half method
;; * get rid of cruft

(defun random-elt (sequence)
  "Returns a random element from a given sequence"
  (nth (random (length sequence)) sequence))

(defun random-grow-form (function-set terminal-set &optional (max-depth 4))
  "Generates a random form of depth 'max-depth' according to the 'grow' mehtod"
  (cons (random-elt function-set)
        (loop repeat 2 ;representing variable arity
              collect (let ((rnd (random 3)))
                        (cond
                          ((and (zerop rnd) (< 0 max-depth)) (random-elt terminal-set)) ; 1 in rnd chance of spawning leaf early
                          ((> max-depth 0) (random-grow-form function-set terminal-set (1- max-depth)))
                          (t (random-elt terminal-set)))))))

(defun random-full-form (function-set terminal-set &optional (max-depth 4))
  "Generates a random form of depth 'max-depth' according to the 'full' method"
  (cons (random-elt function-set)
                (loop repeat 2 ;represents variable arity
                      collect (cond 
                                ((zerop max-depth) (random-elt terminal-set))
                                (t (random-full-form function-set terminal-set (1- max-depth)))))))

(defun run-form (form)
  (let ((*error-output* (make-broadcast-stream)))
    (handler-case (funcall (eval `(lambda ,form)))
      (error () nil))))

(defun create-initial-population (operators operands &key (size 300) (method 'full) (depth 4))
  "Returns a list of random forms of size 'size' and of depth 'depth'"
  (cond
    ((eq method 'full) (loop repeat size
        collect (random-full-form operators operands depth)))
    ((eq method 'grow) (loop repeat size
                             collect (random-grow-form operators operands depth)))
    (t (error (format nil "CREATE-INITIAL-POPULATION: UNKNOWN METHOD ~S" method)))))

(defun fitness (form fitness-fn test-input)
  (loop for input in test-input
        for output = (run-form form input)
        for target = (funcall fitness-fn input)
        for difference = (when output (abs (- target output)))
        for fitness = (when output (/ 1.0 (+ 1 difference)))
        when (null output) do (return-from fitness nil)
        collect fitness into fitness-values
        finally (return (reduce #'* fitness-values))))

(defun traverse-nodes-example (form)
  (labels ((traverse-nodes (subform &optional (indent ""))
                           (loop for node in subform
                                 do (format t "~D:~A ~S~%" (/ (length indent) 2) indent node)
                                 (when (listp node)
                                   (traverse-nodes node
                                                   (concatenate 'string indent "  "))))))
    (traverse-nodes form)))

(defun n-nodes (form)
  (let ((nodes 1))
    (labels ((traverse-nodes (subform)
                             (loop for node in subform
                                   do (incf nodes)
                                   (when (listp node)
                                     (traverse-nodes node)))))
      (traverse-nodes form))
    nodes))

(defun random-node (form)
  (let* ((index 1)
         (nodes-1 (- (n-nodes form) 1))
         (random-node-index (+ (random nodes-1) 1)))
  (labels ((traverse-nodes (subform)
                           (loop for node in subform
                                 do (when (= index random-node-index)
                                      (return-from random-node
                                                   (list :index index :node node)))
                                 (incf index)
                                 (when (listp node)
                                   (traverse-nodes node)))))
    (traverse-nodes form))))

(defun replace-node (form node-index new-node)
  (let ((index 0))
    (labels ((traverse-nodes (subform)
                             (loop for node in subform
                                   do (incf index)
                                   when (= index node-index)
                                     collect new-node
                                     when (and (/= index node-index)
                                                (not (listp node)))
                                       collect node
                                       when (and (/= index node-index)
                                                 (listp node))
                                       collect (traverse-nodes node))))
      (traverse-nodes form))))
                                       

(defun cross-over (form1 form2 &key (debug nil))
  (let ((rnode1 (random-node form1))
        (rnode2 (random-node form2)))
    (when debug
      (format t "form1: ~S~%form2: ~S~%rnode1: ~S~%rnode2: ~S~%"
              form1 form2 rnode1 rnode2))
    (replace-node form1 (getf rnode1 :index) (getf rnode2 :node))))

(defun mutate (form operators &key (debug nil))
  (let ((rform (random-form operators))
        (rnode (random-node form)))
    (when debug
      (format t "form: ~S~%rform: ~S~%rnode: ~S~%" form rform rnode))
    (replace-node form (getf rnode :index) rform)))

(defun evaluate-population (population fitness-fn test-input)
  (loop for form in population
        for fitness = (fitness form fitness-fn test-input)
        when fitness collect (list :fitness fitness :form form) into result
        finally (return (sort result
                              (lambda (a b)
                                (> (getf a :fitness) (getf b :fitness)))))))

(defun head (sequence &optional (amount 1))
  (if (<= amount 0)
    nil
    (if (< (length sequence) amount)
      sequence
      (subseq sequence 0 amount))))

(defun advance-generation (population fitness-fn operators test-input
                                      &optional (max-population 100))
  (let ((epop (evaluate-population population fitness-fn test-input)))
    (format t "Best fitness of current population ~S~%"
            (getf (first epop) :fitness))
    (loop for plist in (head epop max-population)
          for i from 0
          for fitness = (getf plist :fitness)
          for form = (getf plist :form)
          collect form
          when (<= (random 1.0d0) fitness)
          collect (if (<= (random 100) 90)
                    (cross-over form (getf (random-elt epop) :form))
                    (mutate form operators))
          ;; add a new form to the population now and then
          when (<= (random 100) 2) collect (random-form operators))))

